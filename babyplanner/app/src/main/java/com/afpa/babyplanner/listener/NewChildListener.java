package com.afpa.babyplanner.listener;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.afpa.babyplanner.MainActivity;
import com.afpa.babyplanner.NewChildActivity;
import com.afpa.babyplanner.data.Baby;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.view.View.Z;


public class NewChildListener implements View.OnClickListener {

    NewChildActivity main;
    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    public NewChildListener(NewChildActivity main) {
        this.main = main;
    }

    @Override
    public void onClick(View view) {
        double weight;
        double height;
        Date date = null;
        String text="";
        if (main.getTxtName().getText().toString().length() == 0) {
            text = "Entrer un nom !";
            Toast.makeText(main.getApplicationContext(), text, Toast.LENGTH_LONG).show();
        }else if(!main.getTxtName().getText().toString().matches("[a-zA-Z]+([\\-\\s][a-zA-Z]+)*")){
            text = "pas de nombre dans le nom !";
            Toast.makeText(main.getApplicationContext(), text, Toast.LENGTH_LONG).show();
        }else if (main.getTxtFirstName().getText().toString().length() == 0) {
            text = "Entrer un prénom !";
            Toast.makeText(main.getApplicationContext(), text, Toast.LENGTH_LONG).show();
        }else if(!main.getTxtFirstName().getText().toString().matches("[a-zA-Z][a-zA-Z]*(\\-[a-zA-Z]+)?")){
            text = "pas de nombre dans le prénom !";
            Toast.makeText(main.getApplicationContext(), text, Toast.LENGTH_LONG).show();
        }else  if (main.getTxtDate().getText().toString().length() == 0) {
            text = "Entrer une date de naissance !";
            Toast.makeText(main.getApplicationContext(), text, Toast.LENGTH_LONG).show();
        }else if (main.getSpinSex().getSelectedItem().toString().length() == 0) {
            text = "Entrer un sexe !";
            Toast.makeText(main.getApplicationContext(), text, Toast.LENGTH_LONG).show();
        }else if (main.getTxtWeight().getText().toString().length() == 0) {
            text = "Entrer un poids !";
            Toast.makeText(main.getApplicationContext(), text, Toast.LENGTH_LONG).show();
        }else if (main.getTxtHeight().getText().toString().length() == 0) {
            text = "Entrer une taille !";
            Toast.makeText(main.getApplicationContext(), text, Toast.LENGTH_LONG).show();
        }else {
            try {
                weight = Double.parseDouble(main.getTxtWeight().getText().toString());
                if(weight>100){
                    text = "poids limité à 100 kg !";
                    Toast.makeText(main.getApplicationContext(), text, Toast.LENGTH_LONG).show();
                    return;
                }
            } catch (NumberFormatException e) {
                text = "Erreur de poids !";
                Toast.makeText(main.getApplicationContext(), text, Toast.LENGTH_LONG).show();
                return;
            }
            try {
                height = Double.parseDouble(main.getTxtHeight().getText().toString());
                if(height>200) {
                    text = "taille limitée à 200 cm !";
                    Toast.makeText(main.getApplicationContext(), text, Toast.LENGTH_LONG).show();
                    return;
                }
            } catch (NumberFormatException e){
                    text = "Erreur de taille !";
                    Toast.makeText(main.getApplicationContext(), text, Toast.LENGTH_LONG).show();
                    return;
            }
            try{
                date=formatter.parse(main.getTxtDate().getText().toString());
                Baby baby = new Baby(main.getTxtName().getText().toString().trim() , main.getTxtFirstName().getText().toString().trim(),date , main.getSpinSex().getSelectedItem().toString().trim(),
                        weight, height);
                MainActivity.getChildList().addChild(baby);

                try {
                    MainActivity.writeJSON();
                    Intent i = new Intent(main.getApplicationContext(), MainActivity.class);
                    main.startActivity(i);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }catch(ParseException e){
                e.getStackTrace();
            }
        }
    }
}






