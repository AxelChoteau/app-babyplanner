package com.afpa.babyplanner.listener;

import android.support.v7.widget.RecyclerView;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import com.afpa.babyplanner.MainActivity;
import com.afpa.babyplanner.receiver.Receiver;
import com.afpa.babyplanner.vaccine.VaccineActivity;
import com.afpa.babyplanner.viewholder.RecallViewHolder;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class CheckBoxListener implements CompoundButton.OnCheckedChangeListener {

    private VaccineActivity actVaccine;
    private RecallViewHolder vh;
    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    public CheckBoxListener(VaccineActivity actVaccine, RecallViewHolder vh ){
        this.actVaccine=actVaccine;
        this.vh=vh;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int recall=Integer.parseInt(vh.getTextView().getText().toString());


        try {
            actVaccine.getActiveVaccine().setRecall(recall, formatter.parse(vh.txtDate.getText().toString()));
            MainActivity.writeJSON();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e){
            e.printStackTrace();
        }
        Receiver.alarm(actVaccine);
        vh.chkBox.setActivated(true);
        vh.chkBox.setFocusable(false);
    }
}
