package com.afpa.babyplanner.listener;

import android.view.View;
import android.widget.Toast;

import com.afpa.babyplanner.MainActivity;
import com.afpa.babyplanner.WeightActivity;
import com.jjoe64.graphview.series.DataPoint;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WeightCheckListener implements View.OnClickListener {

    private WeightActivity weight;
    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    public WeightCheckListener(WeightActivity weight){
        this.weight=weight;
    }

    @Override
    public void onClick(View v){

        try{
            Date weightDate = formatter.parse(weight.getNewWeightDate().getText().toString());
            Double newWeight = Double.parseDouble(weight.getNewWeight().getText().toString());
            weight.getSeries().appendData(new DataPoint(weightDate, newWeight), true, Integer.MAX_VALUE);
            weight.getBaby().addNewWeight(weightDate, newWeight);

            try {
                MainActivity.writeJSON();
                Toast.makeText(weight.getApplicationContext(),"Le nouveau poids a bien été enregistré",Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch(ParseException e){
            e.getStackTrace();
        }catch (Exception e){
            Toast.makeText(weight.getApplicationContext(),"Erreur dans la sauvegarde du poids",Toast.LENGTH_LONG).show();
        }
    }
}
