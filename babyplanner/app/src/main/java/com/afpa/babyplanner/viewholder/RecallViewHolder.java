package com.afpa.babyplanner.viewholder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.afpa.babyplanner.R;
import com.afpa.babyplanner.adapter.RappelAdapter;
import com.afpa.babyplanner.childinfo.ChildInfoActivity;
import com.afpa.babyplanner.listener.CheckBoxListener;
import com.afpa.babyplanner.listener.DateFocusListener;

import lombok.Getter;

public class RecallViewHolder extends RecyclerView.ViewHolder {
    // each data item is just a string in this case
    RappelAdapter adapt;
    public View view;
    @Getter
    public TextView textView;
    @Getter
    public EditText txtDate;
    @Getter
    public CheckBox chkBox;
    public RecallViewHolder(RappelAdapter adapt, View view) {
        super(view);
        this.view=view;
        this.adapt=adapt;
        textView = itemView.findViewById(R.id.rappel);
        txtDate=itemView.findViewById(R.id.date);
        chkBox=itemView.findViewById(R.id.checkBox);
        chkBox.setOnCheckedChangeListener(new CheckBoxListener(adapt.main, this));
        txtDate.setOnFocusChangeListener(new DateFocusListener(adapt.main.getApplicationContext(), txtDate));
    }
}
