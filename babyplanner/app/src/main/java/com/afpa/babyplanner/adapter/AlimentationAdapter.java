package com.afpa.babyplanner.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afpa.babyplanner.R;
import com.afpa.babyplanner.AlimentationActivity;

import java.util.ArrayList;

public class AlimentationAdapter extends RecyclerView.Adapter<AlimentationAdapter.MyViewHolder> {

    private static AlimentationActivity main;
    private ArrayList<String> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textView;

        public MyViewHolder(TextView v) {
            super(v);
            textView = v;
        }
    }
    // Provide a suitable constructor (depends on the kind of dataset)
    public AlimentationAdapter(AlimentationActivity alim, ArrayList<String> myDataset) {
        mDataset = myDataset;
        this.main=alim;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AlimentationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        TextView v = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.aliment_text_view, parent, false);

        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.textView.setText(mDataset.get(position));
        if(position %2 == 1) {
            holder.itemView.setBackgroundColor(main.getResources().getColor(R.color.lite));
        } else {
            holder.itemView.setBackgroundColor(main.getResources().getColor(R.color.dark));
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
