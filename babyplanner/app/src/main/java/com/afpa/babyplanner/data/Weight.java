package com.afpa.babyplanner.data;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "weight")
public class Weight implements Serializable {

    private Map<Date, Double> weight = new LinkedHashMap<>();


    public Map<Date, Double> getWeight() {
        return weight;
    }

    public void setWeight(Date date, Double weight) {
        this.weight.put(date, weight);
    }
}
