package com.afpa.babyplanner.data;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.view.MenuItem;

import com.afpa.babyplanner.R;

import java.util.List;

import lombok.Getter;


public class MusicService extends Service {

    @Getter
    private static MenuItem sound;
    @Getter
    private static MediaPlayer player;
    @Getter
    public static boolean onPause = false;
    @Getter
    private static final String offMusic = "Play";
    @Getter
    private static final String onMusic = "Pause";

    @Override
    public void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.berceuse);
        player.start();
        player.setLooping(true); // Set looping
        player.setVolume(100, 100);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        player.stop();
        player.release();

    }

    public static boolean pause() {
        if (onPause) {
            player.start();
            return onPause = false;
        } else {
            player.pause();
            return onPause = true;
        }

    }

    public static boolean changeMusicBar(MenuItem item) {
        int id = item.getItemId();
        sound = item;
        boolean choice = false;

        //noinspection SimplifiableIfStatement
        if (id == R.id.help) {
            choice = true;
        }

        if (id == R.id.sound) {
            if (onPause) {
                item.setTitle(onMusic);
            } else {
                item.setTitle(offMusic);
            }


            MusicService.pause();
            choice = true;
        }
        return choice;
    }

    public static void cutMusic(Context context){
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        if (!taskInfo.isEmpty()) {
            ComponentName topActivity = taskInfo.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                player.pause();
            }
        }
    }

    public static boolean restartMusic(){
        if(!onPause){
            player.start();
            return true;
        } else
            return false;



    }
}
