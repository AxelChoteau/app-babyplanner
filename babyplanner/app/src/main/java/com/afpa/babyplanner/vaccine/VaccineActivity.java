package com.afpa.babyplanner.vaccine;


import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.afpa.babyplanner.MainActivity;
import com.afpa.babyplanner.adapter.ChildAdapter;
import com.afpa.babyplanner.adapter.RappelAdapter;
import com.afpa.babyplanner.calendar.NotifyCalendar;
import com.afpa.babyplanner.R;
import com.afpa.babyplanner.adapter.VaccineAdapter;
import com.afpa.babyplanner.data.MusicService;
import com.afpa.babyplanner.listener.DateFocusListener;
import com.afpa.babyplanner.data.Baby;
//import com.afpa.babyplanner.listener.VaccineValidateListener;
//import com.afpa.babyplanner.receiver.Receiver;
import com.afpa.babyplanner.tools.Utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Classe VaccineActivity etends la classe AppCompatActivity, elle affiche la liste des vaccins et la date du dernier vaccin ainsi que la date du prochain.
 * @see AppCompatActivity
 * @author Cassandra
 */

public class VaccineActivity extends AppCompatActivity {
    @Getter
    private Baby baby;
    @Getter
    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    @Getter
    List<Vaccine> listVaccine;
    @Setter
    @Getter
    private Vaccine activeVaccine;
    private RappelAdapter rAdapter;

    /**
     * Cette methode a pour but d'afficher une liste de vaccins,la date du dernier vaccin et la date du prochain vaccin.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaccine);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.baby = MainActivity.getChildList().getChild( getIntent().getIntExtra("baby",0));
//        nextVaccine=findViewById(R.id.textView2);
        final Spinner listName = findViewById(R.id.list_vaccine2);
        listVaccine=baby.getListVaccine();

        activeVaccine = listVaccine.get(0);

        final VaccineActivity unAutreThis = this;
        final RecyclerView rappel = findViewById(R.id.rv_rappel);
        rappel.setHasFixedSize(true);
        rappel.setLayoutManager(new LinearLayoutManager(this));

        listName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                activeVaccine = listVaccine.get(position);
                rAdapter = new RappelAdapter(unAutreThis,activeVaccine.getRecall());
                rappel.setAdapter(rAdapter);
            } // to close the onItemSelected
            public void onNothingSelected(AdapterView<?> parent) { }
        });
        VaccineAdapter adapter = new VaccineAdapter(this, R.layout.my_text_view, listVaccine);
        listName.setAdapter(adapter);
//        lastestVaccine = findViewById(R.id.calendar_lastest_vaccine);
//        lastestVaccine.setOnFocusChangeListener(new DateFocusListener(this,lastestVaccine));

        rAdapter = new RappelAdapter(this,activeVaccine.getRecall());
        rappel.setAdapter(rAdapter);
    }
    @Override
    public void onPause(){
        super.onPause();
        MusicService.restartMusic();
    }
    @Override
    public void onRestart(){
        super.onRestart();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        MusicService.restartMusic();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        boolean choice = false;

        //noinspection SimplifiableIfStatement
        if (id == R.id.help) {
            AlertDialog builder = new AlertDialog.Builder(VaccineActivity.this).create();
            builder.setTitle("Help");
            builder.setMessage("Pour ajouter un vaccin, veuillez sélectionner un vaccin parmi la liste.\n\n" +
                    "Ensuite renseignez la date à laquelle le vaccin a été effectué puis cochez la case.\n\n" +
                    "Une notification vous sera transmise pour valider l'enregistrement du vaccin.\n\n");
            builder.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();

            choice = true;
        }

        if (id == R.id.sound) {
            if (MusicService.isOnPause()) {
                item.setTitle(MusicService.getOnMusic());
            } else {
                item.setTitle(MusicService.getOffMusic());
            }


            MusicService.pause();
            choice = true;
        }
        return choice;

    }
}
