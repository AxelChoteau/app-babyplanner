package com.afpa.babyplanner.vaccine;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Classe RORVaccine etends la classe abstraite Vaccine, elle contiendra toutes les informations necessaire au vaccin ROR.
 * @see Vaccine
 * @author Cassandra
 */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "recall",
        scope = RORVaccine.class)
public class RORVaccine extends Vaccine {

    private static final String NAME="rougeole oreillons rubeole";
    private TreeMap<Integer, Date> recall =new TreeMap<>();

    public RORVaccine(){
        recall.put(12, null);
        recall.put(16, null);
    }

    @Override
    public Map<Integer, Date> getRecall() {
        return recall;
    }

    @Override
    public void setRecall(int month, Date recall) {
        this.recall.put(month, recall);
    }

    @Override
    public String getName() {
        return NAME;
    }


}
