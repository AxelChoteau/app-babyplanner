package com.afpa.babyplanner.listener;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.InputType;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import java.util.Calendar;

public class DateFocusListener implements View.OnFocusChangeListener {

    private Context context;
//    private EditText txtDate;
    private int mYear, mMonth, mDay;

    public DateFocusListener(Context context, EditText txtDate){
        this.context=context;
//        this.txtDate=txtDate;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        final EditText txtDate = (EditText) v;
        if (hasFocus) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txtDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                        }
                    }, mYear, mMonth, mDay);
            txtDate.setInputType(InputType.TYPE_NULL);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
            if(txtDate.getText() != null){
                txtDate.clearFocus();
            }
        }
    }
}
