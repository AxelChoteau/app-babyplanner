package com.afpa.babyplanner.childinfo;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.afpa.babyplanner.MainActivity;
import com.afpa.babyplanner.R;
import com.afpa.babyplanner.data.Baby;
import com.afpa.babyplanner.data.MusicService;
import com.afpa.babyplanner.tools.Utils;
import com.afpa.babyplanner.vaccine.VaccineActivity;

import java.util.Date;
import java.util.List;

import lombok.Getter;

/**
 * Activity ChildInfo, permet d'afficher les information de l'enfant selectionne.
 * @author Sailly
 */

public class ChildInfoActivity extends AppCompatActivity {
    private Button vaccines, alimentation, weight, height;
    @Getter
    private Baby baby;
    private TextView childName, childWeight, childHeight, childAge;
    @Getter
    private int pos;
    /**
     * Methode OnCreate venu de la classe AppCompatActivity, se lance lors de la creation de l'Activity.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_info);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent i=getIntent();
        pos = i.getIntExtra("baby",0);
        baby =  MainActivity.getChildList().getChild(pos);
        /*
        Ajout des textView avec les infos de l'enfant.
         */
        addTextView();

        /*
        Ajout des boutons.
         */

        addButton();

    }

    /**
     * Methode permettant de set les textView avec les informations de l'enfant.
     */
    public void addTextView(){

        childName = findViewById(R.id.child_name);
        childName.setText(baby.getFirstName()+" "+baby.getName());

        childWeight = findViewById(R.id.child_weight);
        childWeight.setText(baby.getCurrentWeight()+" Kg");

        childHeight = findViewById(R.id.child_height);
        childHeight.setText(baby.getCurrentHeight()+" cm");

        childAge = findViewById(R.id.child_age);
        long age=Utils.calculAge(baby.getBirthDate(), new Date());
        if(age<1){
            childAge.setText(Utils.calculAgeJour(baby.getBirthDate(), new Date())+" jours");
        }else{
            childAge.setText(Utils.calculAge(baby.getBirthDate(), new Date())+" mois");
        }


    }

    /**
     * Methode permettant d'ajouter les listener aux boutons de l'Activity.
     */
    public void addButton(){

        vaccines = findViewById(R.id.child_vaccines);
        vaccines.setOnClickListener(new ChildInfoVaccinesListener(this));

        alimentation = findViewById(R.id.child_alimentation);
        alimentation.setOnClickListener(new ChildInfoAlimentationListener(this));

        weight = findViewById(R.id.child_weight_button);
        weight.setOnClickListener(new ChildInfoWeightListener(this));

        height = findViewById(R.id.child_height_button);
        height.setOnClickListener(new ChildInfoHeightListener(this));

    }

    @Override
    public void onPause(){
        super.onPause();
        MusicService.cutMusic(this);
    }
    @Override
    public void onRestart(){
        super.onRestart();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        MusicService.restartMusic();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(MusicService.getPlayer().isPlaying()){
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }else{
            getMenuInflater().inflate(R.menu.menu_main_music_on_pause, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        boolean choice = false;

        //noinspection SimplifiableIfStatement
        if (id == R.id.help) {
            AlertDialog builder = new AlertDialog.Builder(ChildInfoActivity.this).create();
            builder.setTitle("Help");
            builder.setMessage("Les données affichées sont les informations de l'enfant que vous avez ajoutées.\n\n" +
                    "Pour avoir accès à d'autre informations, cliquez sur les boutons vaccin, alimentation, poids, taille.\n\n");
            builder.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();

            choice = true;
        }

        if (id == R.id.sound) {
            if (MusicService.isOnPause()) {
                item.setTitle(MusicService.getOnMusic());
            } else {
                item.setTitle(MusicService.getOffMusic());
            }


            MusicService.pause();
            choice = true;
        }
        return choice;
    }


}
