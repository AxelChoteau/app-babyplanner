package com.afpa.babyplanner.childinfo;

import android.content.Intent;
import android.view.View;

import com.afpa.babyplanner.HeightActivity;

public class ChildInfoHeightListener implements View.OnClickListener {

    private ChildInfoActivity info;

    public ChildInfoHeightListener(ChildInfoActivity info){

        this.info = info;
    }

    @Override
    public void onClick(View v){

        Intent intent = new Intent(info.getApplicationContext(), HeightActivity.class);
        intent.putExtra("baby",info.getPos());
        info.startActivity(intent);
    }
}
