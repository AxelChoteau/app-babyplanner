package com.afpa.babyplanner;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.afpa.babyplanner.adapter.AlimentationAdapter;
import com.afpa.babyplanner.data.Baby;
import com.afpa.babyplanner.data.MusicService;
import com.afpa.babyplanner.tools.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class AlimentationActivity extends AppCompatActivity {

    private static HashMap<String, Integer> listFood = new HashMap<>();
    private RecyclerView allowEating;
    private RecyclerView nextFood;
    private Baby baby;

    static {

        listFood.put("Lait maternel", 1);
        listFood.put("Lait infantil", 1);
        listFood.put("Lait infantil 2eme age", 2);
        listFood.put("Lait de croissance", 5);
        listFood.put("Yaourt", 2);
        listFood.put("Fromage blanc", 3);
        listFood.put("Fromage", 4);

        listFood.put("Legumes verts et carottes cuits", 2);
        listFood.put("Legumes crus", 5);
        listFood.put("Legumes secs et legumineuses", 5);

        listFood.put("Pommes cuits ou ecrases", 2);
        listFood.put("Poires cuits ou ecrases", 2);
        listFood.put("Bananes cuites ou ecrases", 2);
        listFood.put("Peches cuites ou ecrases", 2);
        listFood.put("Abricots cuit ou ecrases", 2);

        listFood.put("Pommes morceaux", 5);
        listFood.put("Poires morceaux", 5);
        listFood.put("Bananes morceaux", 5);
        listFood.put("Peches morceaux", 5);
        listFood.put("Abricots morceaux", 5);

        listFood.put("Fruits exotique ecrases", 4);
        listFood.put("Fruits exotique morceaux", 5);

        listFood.put("Jus de fruits", 5);

        listFood.put("Cereales infantiles sans gluten", 2);
        listFood.put("Cereales infantiles avec gluten", 2);
        listFood.put("Pommes de terre en purée", 2);
        listFood.put("Pommes de terre ecrases", 2);
        listFood.put("Semoule", 2);
        listFood.put("Riz et pates fines", 3);
        listFood.put("Abricots cuit ou ecrases", 4);

        listFood.put("Viande blanche", 2);
        listFood.put("Viande rouge", 5);
        listFood.put("Poisson maigre", 3);
        listFood.put("Poisson gras", 5);
        listFood.put("Oeufs durs, mollets", 3);
        listFood.put("Oeufs à la coque", 5);

        listFood.put("Fruits de mers", 5);
        listFood.put("Beurre", 3);
        listFood.put("Creme fraiche", 3);
        listFood.put("Huile", 6);

        listFood.put("Sel", 6);
        listFood.put("Poivre", 5);

        listFood.put("Biscuits", 6);
        listFood.put("Autres", 5);

        listFood.put("Eau", 1);
        listFood.put("Sodas", 7);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alimentation);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent i = getIntent();
        this.baby = MainActivity.getChildList().getChild( i.getIntExtra("baby",0));
        allowEating = findViewById(R.id.list_food);
        allowEating.setHasFixedSize(true);
        allowEating.setLayoutManager(new LinearLayoutManager(this));
        allowEating.setAdapter(new AlimentationAdapter(this, parcoursMap((int)Utils.calculAge(baby.getBirthDate(), new Date()))));
        nextFood = findViewById(R.id.list_next_food);
        nextFood.setHasFixedSize(true);
        nextFood.setLayoutManager(new LinearLayoutManager(this));
        long a=Utils.calculAge(baby.getBirthDate(), new Date())+1;
        if(a<=1){
            a=2;
        }
        nextFood.setAdapter(new AlimentationAdapter(this, parcoursMap((int)a))); //parcoursMap(baby.getAge()+1);

    }

    private static ArrayList<String> parcoursMap(int a) {

                    if(a==0){
                        a=1;
                    }
                    Iterator<HashMap.Entry<String, Integer>> entries = listFood.entrySet().iterator();
                    ArrayList<String> list = new ArrayList<>();
                    while (entries.hasNext()) {
                        HashMap.Entry<String, Integer> next = entries.next();
                        if (next.getValue() <= a) {
                            list.add(next.getKey());

            }

        }
        return list;

    }

    @Override
    public void onPause(){
        super.onPause();
        MusicService.cutMusic(this);
    }
    @Override
    public void onRestart(){
        super.onRestart();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        MusicService.restartMusic();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(MusicService.getPlayer().isPlaying()){
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }else{
            getMenuInflater().inflate(R.menu.menu_main_music_on_pause, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        boolean choice = false;

        //noinspection SimplifiableIfStatement
        if (id == R.id.help) {
            AlertDialog builder = new AlertDialog.Builder(AlimentationActivity.this).create();
            builder.setTitle("Help");
            builder.setMessage("La première liste d'aliments affichée est les aliments que l'enfant peut dès à présent manger.\n\n" +
                    "La seconde liste d'aliments affichée est les aliments que l'enfant pourra bientôt manger.\n\n");
            builder.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();

            choice = true;
        }

        if (id == R.id.sound) {
            if (MusicService.isOnPause()) {
                item.setTitle(MusicService.getOnMusic());
            } else {
                item.setTitle(MusicService.getOffMusic());
            }


            MusicService.pause();
            choice = true;
        }
        return choice;
    }
}


