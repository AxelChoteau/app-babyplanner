package com.afpa.babyplanner;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import com.afpa.babyplanner.data.MusicService;
import com.afpa.babyplanner.listener.DateFocusListener;
import com.afpa.babyplanner.listener.HeightCheckListener;
import com.afpa.babyplanner.data.Baby;
import com.afpa.babyplanner.tools.Utils;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.SimpleDateFormat;
import java.util.List;

import lombok.Getter;

public class HeightActivity extends AppCompatActivity {

    private Button validate;
    @Getter
    private EditText newHeightDate;
    @Getter
    private EditText newHeight;
    @Getter
    private Baby baby;
    @Getter
    private LineGraphSeries<DataPoint> series;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.height_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent i = getIntent();
        this.baby = MainActivity.getChildList().getChild( i.getIntExtra("baby",0));


        validate = findViewById(R.id.height_activity_validate);
        validate.setOnClickListener(new HeightCheckListener(this));

        newHeightDate = findViewById(R.id.new_height_date);
        newHeightDate.setOnFocusChangeListener(new DateFocusListener(this, newHeightDate));

        newHeight = findViewById(R.id.new_height);
        newHeight.requestFocus();

        GraphView graph = (GraphView) findViewById(R.id.height_graph);
        series = new LineGraphSeries<>(Utils.mapToData(baby.getHeight().getHeight()));
        graph.addSeries(series);
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    return sdf.format(value);
                } else {
                    return super.formatLabel(value, isValueX) + " cm";
                }
            }
        });
        graph.getGridLabelRenderer().setNumHorizontalLabels(3);

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX((double) baby.getBirthDate().getTime());
//        graph.getViewport().setMaxX(Double.MAX_VALUE);
        graph.getViewport().setScalable(true); // enables horizontal zooming and scrolling
        graph.getViewport().setScrollable(true); // enables horizontal scrolling
        graph.addSeries(series);

    }

    @Override
    public void onPause(){
        super.onPause();
        MusicService.cutMusic(this);
    }

    @Override
    public void onRestart(){
        super.onRestart();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        MusicService.restartMusic();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(MusicService.getPlayer().isPlaying()){
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }else{
            getMenuInflater().inflate(R.menu.menu_main_music_on_pause, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        boolean choice = false;

        //noinspection SimplifiableIfStatement
        if (id == R.id.help) {
            AlertDialog builder = new AlertDialog.Builder(HeightActivity.this).create();
            builder.setTitle("Help");
            builder.setMessage("Veuillez ajouter une taille et une date avant d'appuyer sur valider.\n\n" +
                    "La prochaine fois que vous retournerez sur la taille de l'enfant, la courbe sera modifiée.\n\n");
            builder.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();

            choice = true;
        }

        if (id == R.id.sound) {
            if (MusicService.isOnPause()) {
                item.setTitle(MusicService.getOnMusic());
            } else {
                item.setTitle(MusicService.getOffMusic());
            }


            MusicService.pause();
            choice = true;
        }
        return choice;
    }
}
