package com.afpa.babyplanner.tools;

import com.jjoe64.graphview.series.DataPoint;


import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Utils {

    private Utils(){}

    public static DataPoint[] mapToData(Map<Date, Double> map){
        DataPoint[] data = new DataPoint[map.size()];
        Set<Map.Entry<Date, Double>> entry = map.entrySet();
        Iterator<Map.Entry<Date, Double>> iter = entry.iterator();
        int i=0;
        while (iter.hasNext()){
            Map.Entry next = iter.next();
            data[i]= new DataPoint((Date) next.getKey(), Double.parseDouble(next.getValue().toString()));
            i++;
        }
        return data;
    }

    public static long calculAge(Date birth,Date j){

        long ms=birth.getTime()-j.getTime();
        long res =  ms/(1000*60*60*24*30)-1;
        if(res < 0){
            res = 0;
        }
        return   res;

    }
    public static long calculAgeJour(Date birth,Date j){

        long ms=birth.getTime()-j.getTime();
        long res =  ms/(1000*60*60*24);
        if(res < 0){
            res = 0;
        }
        return   res;

    }
    public static long calculJour(Date birth,Date activeVaccine){
        long ms=birth.getTime()-activeVaccine.getTime();
        return   ms/(1000*60*60*24);

    }
}