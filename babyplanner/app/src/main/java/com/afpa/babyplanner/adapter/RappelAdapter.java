package com.afpa.babyplanner.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.afpa.babyplanner.MainActivity;
import com.afpa.babyplanner.R;
import com.afpa.babyplanner.childinfo.ChildInfoActivity;
import com.afpa.babyplanner.listener.CheckBoxListener;
import com.afpa.babyplanner.listener.DateFocusListener;
import com.afpa.babyplanner.vaccine.VaccineActivity;
import com.afpa.babyplanner.viewholder.RecallViewHolder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import lombok.Getter;

public class RappelAdapter extends RecyclerView.Adapter<RecallViewHolder>{
    public VaccineActivity main;
    private Map<Integer, Date> mDataset;
    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
//    public static class MyViewHolder extends RecyclerView.ViewHolder {
//        // each data item is just a string in this case
//        @Getter
//        public TextView textView;
//        @Getter
//        public EditText txtDate;
//        @Getter
//        public CheckBox chkBox;
//        public MyViewHolder(TextView v, EditText d, CheckBox c) {
//            super(v);
//            textView = v;
//            txtDate=d;
//            chkBox=c;
//            chkBox.setOnCheckedChangeListener(new CheckBoxListener(RappelAdapter.main, this));
//            txtDate.setOnFocusChangeListener(new DateFocusListener(RappelAdapter.main.getApplicationContext(), txtDate));
//            textView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent i = new Intent(main.getApplicationContext(), ChildInfoActivity.class);
//                    i.putExtra("baby",getAdapterPosition() );
//                    main.startActivity(i);
//                }
//            });
//        }
//    }
    // Provide a suitable constructor (depends on the kind of dataset)
    public RappelAdapter(VaccineActivity main, Map<Integer, Date> myDataset) {
        mDataset = myDataset;
        this.main=main;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecallViewHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vaccine_recall_view, parent, false);

        RecallViewHolder vh = new RecallViewHolder(this, v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecallViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Integer[] key = mDataset.keySet().toArray(new Integer[mDataset.size()]);
        int rec  = key[position];
        holder.textView.setText(rec+"");
        holder.textView.setTextColor(main.getResources().getColor(R.color.colorFont_menu));
        Date inst;
        if((inst=mDataset.get(rec))!=null){
            holder.txtDate.setText(formatter.format(inst));
            holder.txtDate.setFocusable(false);
            holder.chkBox.setActivated(true);
            holder.chkBox.setFocusable(false);
        } else {
            holder.txtDate.setHint("Date du vaccin");
            holder.chkBox.setActivated(false);
        }
        if(position %2 == 1) {
            holder.itemView.setBackgroundColor(main.getResources().getColor(R.color.lite));
        } else {
            holder.itemView.setBackgroundColor(main.getResources().getColor(R.color.dark));
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
