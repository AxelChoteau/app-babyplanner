package com.afpa.babyplanner.listener;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.afpa.babyplanner.HeightActivity;
import com.afpa.babyplanner.MainActivity;
import com.afpa.babyplanner.childinfo.ChildInfoActivity;
import com.jjoe64.graphview.series.DataPoint;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HeightCheckListener implements View.OnClickListener {

    private HeightActivity height;
    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    public HeightCheckListener(HeightActivity height){
        this.height=height;
    }

    @Override
    public void onClick(View v){

        try{
            Date heightDate = formatter.parse(height.getNewHeightDate().getText().toString());
            Double newHeight = Double.parseDouble(height.getNewHeight().getText().toString());
            height.getSeries().appendData(new DataPoint(heightDate, newHeight), true, Integer.MAX_VALUE);
            height.getBaby().addNewHeight(heightDate, newHeight);

            try {
                MainActivity.writeJSON();
                Toast.makeText(height.getApplicationContext(),"La nouvelle taille a bien été enregistrée", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch(ParseException e){
            e.getStackTrace();
        }catch (Exception e){
            Toast.makeText(height.getApplicationContext(),"Erreur dans la sauvegarde de la taille", Toast.LENGTH_LONG).show();
        }
    }
}
