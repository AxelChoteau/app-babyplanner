package com.afpa.babyplanner.vaccine;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;


public class Vaccines {



    @Getter
    private static ArrayList<Vaccine> listVaccines= new ArrayList<>();


    static {
        listVaccines.add(new DTPVaccine());
        listVaccines.add(new CoquelucheVaccine());
        listVaccines.add(new HepatiteBVaccine());
        listVaccines.add(new MeningocoqueCVaccine());
        listVaccines.add(new PneumocoqueVaccine());
        listVaccines.add(new RORVaccine());
        listVaccines.add(new HIBVaccine());
    }

    /**
     * Cette methode a pour but de retourner une liste de String à partir de la liste de vaccins que contient la classe.
     * @return ArrayList<String>
     */
    public static List<String> listNameVaccine(){
        List<String> listNameV= new ArrayList<>();
        for(int i=0;i<listVaccines.size();i++){
            listNameV.add(listVaccines.get(i).getName());
        }        return listNameV;
    }

}
