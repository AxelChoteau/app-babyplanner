package com.afpa.babyplanner;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.afpa.babyplanner.data.MusicService;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.afpa.babyplanner.adapter.ChildAdapter;
import com.afpa.babyplanner.data.ChildList;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;


import lombok.Getter;

public class MainActivity extends AppCompatActivity {

    @Getter
    private static ChildList childList;
    @Getter
    private static File file;
    @Getter
    private static Intent backgroundMusic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        file= new File(getApplicationContext().getFilesDir(), "babyplanner.xml");

        try {
            childList = readJSON();
        } catch (IOException e) {
            e.printStackTrace();
        }
        backgroundMusic = new Intent(this, MusicService.class);
            startService(backgroundMusic);

        RecyclerView children = findViewById(R.id.main_list_child);
        children.setHasFixedSize(true);
        children.setLayoutManager(new LinearLayoutManager(this));
        ChildAdapter adapter =new ChildAdapter(this,childList.getChildrenName());
        children.setAdapter(adapter);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), NewChildActivity.class);
                startActivity(i);
            }
        });
    }


    @Override
    public void onPause(){
        super.onPause();
        MusicService.cutMusic(this);
    }
    @Override
    public void onRestart(){
        super.onRestart();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        MusicService.restartMusic();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(MusicService.getPlayer().isPlaying()){
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }else{
            getMenuInflater().inflate(R.menu.menu_main_music_on_pause, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        boolean choice = false;

        //noinspection SimplifiableIfStatement
        if (id == R.id.help) {
            AlertDialog builder = new AlertDialog.Builder(MainActivity.this).create();
            builder.setTitle("Help");
            builder.setMessage("Dans un premier temps, la page est vide.\n\n" +
                    "Pour ajouter un enfant, veuillez appuyer sur le + en bas à droite de la page.\n\n" +
                    "Une fois le premier enfant enregistré, vous pourrez appuyer sur son prénom pour afficher les diverses informations le concernant.\n\n");
            builder.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();

            choice = true;
        }

        if (id == R.id.sound) {
            if (MusicService.isOnPause()) {
                item.setTitle(MusicService.getOnMusic());
            } else {
                item.setTitle(MusicService.getOffMusic());
            }


            MusicService.pause();
            choice = true;
        }
        return choice;
    }

    public static void writeJSON() throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping();
        mapper.writeValue(file, childList);
    }

    public ChildList readJSON() throws IOException{
        if (file.exists()){
            ObjectMapper mapper = new ObjectMapper();
            mapper.enableDefaultTyping();
            return mapper.readValue(file, ChildList.class);
        }else {
            return new ChildList();
        }
    }
}
