package com.afpa.babyplanner.data;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.ArrayList;
import java.util.List;


import lombok.Getter;
import lombok.Setter;

/**
 * @author Choteau
 */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "childList")
public class ChildList {

    /**
     * Liste des enfants ajoutes dans l'application
     */
    @Getter
    @Setter
    private List<Baby> childList;

    /**
     * Constructeur sans parametre, initialise la liste des enfants
     */
    public ChildList (){
        childList=new ArrayList<>();
    }

    /**
     * Methode permettant d'ajouter un enfant a la liste
     *
     * @param child : enfant a ajouter a la liste
     */
    public void addChild(Baby child){
        childList.add(child);
    }

    /**
     * Methode permettant de recuperer un enfant dans la liste
     *
     * @param i index de l'enfant a recuperer
     * @return l'enfant a l'index i dans la liste
     */
    public Baby getChild(int i){
        return childList.get(i);
    }

    /**
     *
     * @return un tableau de String avec le nom des enfants dans la base de donnee.
     */
    @JsonIgnore
    public String[] getChildrenName(){
        String[] names=new String[childList.size()];
        for (int i=0; i<childList.size();i++){
            names[i]=childList.get(i).getName()+" "+childList.get(i).getFirstName();
        }
        return names;
    }

}
