package com.afpa.babyplanner.calendar;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;

import com.afpa.babyplanner.vaccine.Vaccine;
import com.afpa.babyplanner.vaccine.VaccineActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class NotifyCalendar {

     public static void setAlarm(int dec, Date birth,String title,String desc){
         Intent intent = new Intent(Intent.ACTION_INSERT);
         intent.setType("vnd.android.cursor.item/event");
         intent.putExtra(CalendarContract.Events.TITLE, title);
         intent.putExtra(CalendarContract.Events.DESCRIPTION, desc);

         // Setting dates
         SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
         Calendar calDate = getCalendar(birth);
         int month = calDate.get(Calendar.MONTH);
         int year = calDate.get(Calendar.YEAR);
         int day = calDate.get(Calendar.DAY_OF_MONTH);
         month+=dec;
         if (month>12){
             month-=12;
             year++;
         }
         calDate.set(year,month,day);
         intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                 calDate.getTimeInMillis());
         intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                 calDate.getTimeInMillis());

         // make it a full day event
         intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
     }

    public static Calendar getCalendar(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal;
    }

    public static void setAlarm2(Activity vaccine, int dec, Date birth){
        Calendar calDate = getCalendar(birth);
        int month = calDate.get(Calendar.MONTH);
        int year = calDate.get(Calendar.YEAR);
        int day = calDate.get(Calendar.DAY_OF_MONTH);
        month+=dec;
        if (month>12){
            month-=12;
            year++;
        }
        AlarmManager objAlarmManager = (AlarmManager) vaccine.getSystemService(Context.ALARM_SERVICE);
        Calendar objCalendar = Calendar.getInstance();
        objCalendar.set(Calendar.YEAR, 2018);
        //objCalendar.set(Calendar.YEAR, objCalendar.get(Calendar.YEAR));
        objCalendar.set(Calendar.MONTH, 2);
        objCalendar.set(Calendar.DAY_OF_MONTH, 1);
        objCalendar.set(Calendar.HOUR_OF_DAY, 9);
        objCalendar.set(Calendar.MINUTE, 41);
        objCalendar.set(Calendar.SECOND, 0);
        objCalendar.set(Calendar.MILLISECOND, 0);
        objCalendar.set(Calendar.AM_PM, Calendar.AM);

        Intent alamShowIntent = new Intent(vaccine, VaccineActivity.class);
        PendingIntent alarmPendingIntent = PendingIntent.getActivity(vaccine, 0,alamShowIntent,0 );

        objAlarmManager.set(AlarmManager.RTC_WAKEUP,objCalendar.getTimeInMillis(), alarmPendingIntent);
    }
}
