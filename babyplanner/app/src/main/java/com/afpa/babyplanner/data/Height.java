package com.afpa.babyplanner.data;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "height")
public class Height implements Serializable {
    private Map<Date, Double> height = new LinkedHashMap<>();

    public Map<Date, Double> getHeight() {
        return height;
    }

    public void setHeight(Date date, Double height) {
        this.height.put(date, height);
    }
}
