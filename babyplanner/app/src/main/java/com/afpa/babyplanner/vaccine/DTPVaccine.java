package com.afpa.babyplanner.vaccine;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import lombok.Getter;
import lombok.Setter;


/**
 * Classe DTPVaccine etends la classe abstraite Vaccine, elle contiendra toutes les informations necessaire au vaccin DTP.
 * @see Vaccine
 * @author Cassandra
 */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "recall",
        scope = DTPVaccine.class)
public class DTPVaccine extends Vaccine {

    private static final String NAME = "diphterie tetanos poliomyelite";
    private TreeMap<Integer, Date> recall=new TreeMap<>();

    public DTPVaccine(){
        recall.put(2, null);
        recall.put(4, null);
        recall.put(11, null);
        recall.put(72, null);
        recall.put(132, null);
        recall.put(300, null);
        recall.put(540, null);
        recall.put(780, null);

    }

    @Override
    public Map<Integer, Date> getRecall() {
        return recall;
    }

    @Override
    public void setRecall(int month, Date recall) {
        this.recall.put(month, recall);
    }

    @Override
    public String getName() {
        return NAME;
    }




}
