package com.afpa.babyplanner;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.afpa.babyplanner.data.MusicService;
import com.afpa.babyplanner.listener.DateFocusListener;
import com.afpa.babyplanner.listener.NewChildListener;

import java.util.List;

import lombok.Getter;


public class NewChildActivity extends AppCompatActivity{

    Button validate;
    @Getter
    EditText txtDate;
    @Getter
    EditText txtName;
    @Getter
    EditText txtFirstName;
    @Getter
    EditText txtWeight;
    @Getter
    EditText txtHeight;
    @Getter
    Spinner spinSex;
    private String[] sex_list = {"male","female"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_child);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        validate =findViewById(R.id.newchild_validate);
        txtDate=findViewById(R.id.newchild_date_of_birth);
        txtFirstName=findViewById(R.id.newchild_firstname);
        txtName=findViewById(R.id.newchild_name);
        txtHeight=findViewById(R.id.newchild_height);
        txtWeight= findViewById(R.id.newchild_weight);
        spinSex = findViewById(R.id.newchild_sex);
        validate.setOnClickListener(new NewChildListener(this));
        txtDate.setOnFocusChangeListener(new DateFocusListener(this,txtDate));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, sex_list);
        spinSex.setAdapter(adapter);
    }
    @Override
    public void onPause(){
        super.onPause();
        MusicService.cutMusic(this);
    }
    @Override
    public void onRestart(){
        super.onRestart();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        MusicService.restartMusic();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(MusicService.getPlayer().isPlaying()){
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }else{
            getMenuInflater().inflate(R.menu.menu_main_music_on_pause, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        boolean choice = false;

        //noinspection SimplifiableIfStatement
        if (id == R.id.help) {
            AlertDialog builder = new AlertDialog.Builder(NewChildActivity.this).create();
            builder.setTitle("Help");
            builder.setMessage("Pour ajouter un enfant, veuillez renseigner le nom, le prénom, la date de naissance, le sexe, le poids et la taille de l'enfant.\n\n" +
                    "Ensuite appuyez sur valider.\n\n" +
                    "Une fois terminée, l'enfant sera ajouté à la page principale.\n\n");
            builder.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();

            choice = true;
        }

        if (id == R.id.sound) {
            if (MusicService.isOnPause()) {
                item.setTitle(MusicService.getOnMusic());
            } else {
                item.setTitle(MusicService.getOffMusic());
            }


            MusicService.pause();
            choice = true;
        }
        return choice;
    }
}
