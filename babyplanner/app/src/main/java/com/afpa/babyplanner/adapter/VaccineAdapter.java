package com.afpa.babyplanner.adapter;


import android.app.Activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afpa.babyplanner.R;

import com.afpa.babyplanner.vaccine.Vaccine;
import com.afpa.babyplanner.vaccine.Vaccines;

import java.util.List;

public class VaccineAdapter extends ArrayAdapter<Vaccine> {

    LayoutInflater flater;
    Activity context;
    public VaccineAdapter(Activity context, int textviewId, List<Vaccine> list){

        super(context,textviewId, list);
        this.context=context;
//        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return rowview(convertView,position, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowview(convertView,position, parent ) ;
    }

    private View rowview(View convertView , int position, ViewGroup parent){

        MyViewHolder holder ;
    final int pos = position;
        if (convertView==null) {


            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = flater.inflate(R.layout.spinner_text_view_vaccine, parent, false);

            holder = new MyViewHolder();
            holder.txtView = (TextView) convertView.findViewById(R.id.textView3);
            holder.txtView.setTextColor(context.getResources().getColor(R.color.colorFont));


            convertView.setTag(holder);
        }else{
            holder = (MyViewHolder) convertView.getTag();
        }
//        Vaccine rowItem = context.getListVaccine().get(position);
        Vaccine rowItem = Vaccines.getListVaccines().get(position);
        holder.txtView.setText(rowItem.getName());

        return convertView;
    }

    private class MyViewHolder{
        TextView txtView;
    }

}

