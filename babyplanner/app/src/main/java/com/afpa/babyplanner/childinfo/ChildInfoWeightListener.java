package com.afpa.babyplanner.childinfo;

import android.content.Intent;
import android.view.View;

import com.afpa.babyplanner.WeightActivity;

public class ChildInfoWeightListener implements View.OnClickListener {

    private ChildInfoActivity info;

    public ChildInfoWeightListener(ChildInfoActivity info){

        this.info = info;
    }

    @Override
    public void onClick(View v){
        Intent intent = new Intent(info.getApplicationContext(), WeightActivity.class);
        intent.putExtra("baby",info.getPos());
        info.startActivity(intent);
    }
}
