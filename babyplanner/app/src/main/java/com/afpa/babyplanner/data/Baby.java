package com.afpa.babyplanner.data;

import com.afpa.babyplanner.vaccine.CoquelucheVaccine;
import com.afpa.babyplanner.vaccine.DTPVaccine;
import com.afpa.babyplanner.vaccine.HIBVaccine;
import com.afpa.babyplanner.vaccine.HepatiteBVaccine;
import com.afpa.babyplanner.vaccine.MeningocoqueCVaccine;
import com.afpa.babyplanner.vaccine.PneumocoqueVaccine;
import com.afpa.babyplanner.vaccine.RORVaccine;
import com.afpa.babyplanner.vaccine.Vaccine;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lombok.Getter;
import lombok.Setter;
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "name")
public class Baby implements Serializable {


    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String firstName;
    @Getter
    @Setter
    private Date birthDate;
    @Getter
    @Setter
    private String sex;
    @Getter
    @Setter
    private double currentWeight;
    @Getter
    @Setter
    private double currentHeight;
    @Setter
    @Getter
    private Weight weight =new Weight();;
    @Setter
    @Getter
    private Height height =new Height();;
    @Getter
    @Setter
    private List<Vaccine> listVaccine=new ArrayList<>();

    public Baby(){

    }

    public Baby(String name, String firstName, Date birthDate, String sex, double weight, double height){
        this.name = name;
        this.firstName = firstName;
        this.birthDate = birthDate;
        this.sex = sex;
        this.weight.setWeight(birthDate,weight);
        this.height.setHeight(birthDate,height);
        this.currentWeight=weight;
        this.currentHeight=height;
        listVaccine.add(new DTPVaccine());
        listVaccine.add(new CoquelucheVaccine());
        listVaccine.add(new HepatiteBVaccine());
        listVaccine.add(new MeningocoqueCVaccine());
        listVaccine.add(new PneumocoqueVaccine());
        listVaccine.add(new RORVaccine());
        listVaccine.add(new HIBVaccine());
    }



    public void addNewWeight(Date date, double weight){
        this.currentWeight=weight;
        this.weight.setWeight(date, weight);

    }

    public void addNewHeight(Date date, double height){
        this.currentHeight=height;
        this.height.setHeight(date, height);

    }
}


