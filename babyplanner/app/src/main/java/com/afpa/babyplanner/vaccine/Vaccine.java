package com.afpa.babyplanner.vaccine;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
    * Classe Abstraite permettant aux classes filles de recevoir les attributs et methodes dediees aux vaccins.
    * @author Cassandra
     */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "recall")
public abstract class Vaccine implements Serializable {




    /**
     * Cette methode permettra de definir la date du prochain vaccin.
     * @param latest correspond au dernier vaccin effectuée
     * @return int correspond au prochain vaccine_recall_view à effectuer
     */

    public static int dateNextVaccine(int latest, List<Integer> recall){

        for (int  i = 0; i < recall.size(); i++) {


            if (recall.get(i) == latest) {


                return recall.get(i+1);
            }
        }
        return -1;
    }

    @JsonIgnore
    public abstract String getName();

    public static long ageBabyNextVaccine(Date birth,Date activeVaccine){
        long ms=birth.getTime()-activeVaccine.getTime();
        return   ms/(1000*60*60*24);

    }

    public abstract Map<Integer, Date> getRecall();
    public abstract void setRecall(int month, Date recall);

}
