package com.afpa.babyplanner.childinfo;

import android.content.Intent;
import android.view.View;


import com.afpa.babyplanner.vaccine.VaccineActivity;



public class ChildInfoVaccinesListener implements View.OnClickListener{

    private ChildInfoActivity info;

    public ChildInfoVaccinesListener(ChildInfoActivity info){

        this.info = info;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(info.getApplicationContext(), VaccineActivity.class);
        intent.putExtra("baby",info.getPos());
        info.startActivity(intent);

    }
}
