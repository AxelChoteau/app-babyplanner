package com.afpa.babyplanner.childinfo;

import android.content.Intent;
import android.view.View;

import com.afpa.babyplanner.AlimentationActivity;

public class ChildInfoAlimentationListener implements View.OnClickListener {

    private ChildInfoActivity info;

    public ChildInfoAlimentationListener(ChildInfoActivity info){

        this.info = info;
    }

    @Override
    public void onClick(View v){
        Intent intent = new Intent(info.getApplicationContext(), AlimentationActivity.class);
        intent.putExtra("baby",info.getPos());
        info.startActivity(intent);
    }
}
